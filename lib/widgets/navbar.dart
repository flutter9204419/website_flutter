import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:website_demo/utils/colors.dart';
import 'package:website_demo/utils/image_text.dart';
import 'package:website_demo/utils/style.dart';

class NavBar extends StatefulWidget {
  const NavBar({super.key});

  @override
  State<NavBar> createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      mobile: (context) => mobileNavBar(),
      desktop: (context) => desktopNavBar(20,4),
      tablet: (context) => desktopNavBar(10,0),
    );
  }

  Widget mobileNavBar() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      height: 70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Icon(Icons.menu),
          navLogo(),
        ],
      ),
    );
  }

  Widget desktopNavBar(double margin, double textMargin) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      height: 70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          navLogo(),
          Row(
            children: [
              navButton("Features", textMargin),
              navButton("About us", textMargin),
              navButton("Pricing", textMargin),
              navButton("Feedback", textMargin),
            ],
          ),
          SizedBox(
            height: 45,
            child: ElevatedButton(
              onPressed: () {},
              style: borderButtonStyle,
              child: Text(
                'Request a demo',
                style: TextStyle(
                  color: AppColors.primary,
                  fontSize: 16,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget navLogo() {
    return Image.asset(
      AppImage.logo,
      width: 112,
      height: 40,
    );
  }

  Widget navButton(String text,double margin) {
    return Container(
      margin:  EdgeInsets.symmetric(horizontal: margin),
      child: TextButton(
        onPressed: () {},
        child: Text(
          text,
          style: const TextStyle(
            color: Colors.black,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
