import 'package:flutter/material.dart';
import 'package:website_demo/utils/colors.dart';
import 'package:website_demo/utils/constants.dart';

Widget commonContainer(String s1, s2, s3, image, bool imageLeft) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: w! / 6, vertical: h! / 10),
    color: Colors.white,
    child: Row(
      children: [
        imageLeft
            ? Expanded(
                child: Container(
                height: 300,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(image),
                    fit: BoxFit.contain,
                  ),
                ),
              ))
            : Container(),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(
                right: imageLeft ? 0 : 50, left: imageLeft ? 50 : 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  s1.toUpperCase(),
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey.shade400,
                  ),
                ),
                const SizedBox(height: 20),
                Text(
                  s2,
                  style: TextStyle(
                    fontSize: w! / 25,
                    height: 1.1,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 20),
                Text(
                  s3,
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey.shade400,
                  ),
                ),
                const SizedBox(height: 40),
                TextButton(
                    onPressed: () {},
                    child: Row(
                      children: [
                        Text(
                          'Learn More',
                          style: TextStyle(color: AppColors.primary),
                        ),
                        const SizedBox(width: 10),
                        Icon(
                          Icons.arrow_forward_ios_outlined,
                          size: 15,
                          color: AppColors.primary,
                        ),
                      ],
                    ))
              ],
            ),
          ),
        ),
        !imageLeft
            ? Expanded(
                child: Container(
                height: 300,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(image),
                    fit: BoxFit.contain,
                  ),
                ),
              ))
            : Container(),
      ],
    ),
  );
}

Widget commonContainerMobile(String s1, s2, s3, image) {
  return Container(
    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
    color: Colors.white,
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          height: 300,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(image),
              fit: BoxFit.contain,
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              s1.toUpperCase(),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
                color: Colors.grey.shade400,
              ),
            ),
            const SizedBox(height: 20),
            Text(
              s2,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: w! / 12,
                height: 1.1,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 20),
            Text(
              s3,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
                color: Colors.grey.shade400,
              ),
            ),
            const SizedBox(height: 30),
            TextButton(
                onPressed: () {},
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Learn More',
                      style: TextStyle(color: AppColors.primary),
                    ),
                    const SizedBox(width: 10),
                    Icon(
                      Icons.arrow_forward_ios_outlined,
                      size: 15,
                      color: AppColors.primary,
                    ),
                  ],
                ))
          ],
        ),
      ],
    ),
  );
}
