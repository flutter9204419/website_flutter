import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:website_demo/utils/constants.dart';

class CopyRight extends StatefulWidget {
  const CopyRight({super.key});

  @override
  State<CopyRight> createState() => _CopyRightState();
}

class _CopyRightState extends State<CopyRight> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      desktop: (context) => desktopCopyRight(),
      mobile: (context) => mobileCopyRight(),
      tablet: (context) => desktopCopyRight(),
    );
  }

  Widget mobileCopyRight() {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: const Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Privacy & Terms"),
              SizedBox(width: 10),
              Text("Contact Us"),
            ],
          ),
          Text("Copyright @ 2023 Shreeji")
        ],
      ),
    );
  }

  Widget desktopCopyRight() {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: w! / 6, vertical: 20),
      child: const Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text("Privacy & Terms"),
              SizedBox(width: 10),
              Text("Contact Us"),
            ],
          ),
          Text("Copyright @ 2023 Shreeji")
        ],
      ),
    );
  }
}
