import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:website_demo/utils/constants.dart';
import 'package:website_demo/utils/image_text.dart';

class Footer extends StatefulWidget {
  const Footer({super.key});

  @override
  State<Footer> createState() => _FooterState();
}

class _FooterState extends State<Footer> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      desktop: (context) => desktopFooter(),
      mobile: (context) => mobileFooter(),
      tablet: (context) => tabletFooter(),
    );
  }

  Widget mobileFooter() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(
            AppImage.logo,
            height: 40,
            width: 120,
            fit: BoxFit.contain,
          ),
          const SizedBox(height: 40),
          Text(
            "Links".toUpperCase(),
            style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 20),
          footerText("Home"),
          footerText("About Us"),
          footerText("Careers"),
          footerText("Pricing"),
          footerText("Features"),
          footerText("Blog"),
          const SizedBox(height: 20),
          Text(
            "Legal".toUpperCase(),
            style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 20),
          footerText("Terms of Use"),
          footerText("Teams of conditions"),
          footerText("Privacy Policy"),
          footerText("Cookie Policy"),
        ],
      ),
    );
  }

  Widget tabletFooter() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: w! / 6, vertical: h! / 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            AppImage.logo,
            height: 40,
            width: 120,
            fit: BoxFit.contain,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Links".toUpperCase(),
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 20),
              footerText("Home"),
              footerText("About Us"),
              footerText("Careers"),
              footerText("Pricing"),
              footerText("Features"),
              footerText("Blog"),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Legal".toUpperCase(),
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 20),
              footerText("Terms of Use"),
              footerText("Teams of conditions"),
              footerText("Privacy Policy"),
              footerText("Cookie Policy"),
            ],
          )
        ],
      ),
    );
  }

  Widget desktopFooter() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: w! / 6, vertical: h! / 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            AppImage.logo,
            height: 40,
            width: 120,
            fit: BoxFit.contain,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Links".toUpperCase(),
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 20),
              footerText("Home"),
              footerText("About Us"),
              footerText("Careers"),
              footerText("Pricing"),
              footerText("Features"),
              footerText("Blog"),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Legal".toUpperCase(),
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 20),
              footerText("Terms of Use"),
              footerText("Teams of conditions"),
              footerText("Privacy Policy"),
              footerText("Cookie Policy"),
            ],
          )
        ],
      ),
    );
  }

  Text footerText(String text) {
    return Text(
      text,
      style: const TextStyle(fontSize: 16),
    );
  }
}
