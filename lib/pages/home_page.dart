import 'package:flutter/material.dart';
import 'package:website_demo/pages/containers/container1.dart';
import 'package:website_demo/pages/containers/container2.dart';
import 'package:website_demo/pages/containers/container3.dart';
import 'package:website_demo/pages/containers/container4.dart';
import 'package:website_demo/pages/containers/container5.dart';
import 'package:website_demo/utils/constants.dart';
import 'package:website_demo/widgets/copyright.dart';
import 'package:website_demo/widgets/footer.dart';
import 'package:website_demo/widgets/navbar.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    w = MediaQuery.of(context).size.width;
    h = MediaQuery.of(context).size.height;
    return const Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            NavBar(),
            Container1(),
            Container2(),
            Container3(),
            Container4(),
            Container5(),
            Footer(),
            CopyRight(),
          ],
        ),
      ),
    );
  }
}
