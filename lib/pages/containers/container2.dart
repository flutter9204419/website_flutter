import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:website_demo/utils/colors.dart';
import 'package:website_demo/utils/constants.dart';
import 'package:website_demo/utils/image_text.dart';

class Container2 extends StatefulWidget {
  const Container2({super.key});

  @override
  State<Container2> createState() => _Container2State();
}

class _Container2State extends State<Container2> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      desktop: (context) => desktopContainer2(),
      mobile: (context) => mobileContainer2(),
      tablet: (context) => tabletContainer2(),
    );
  }

  Widget mobileContainer2() {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(color: AppColors.primary),
      child: Column(
        children: [
          Container(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 0),
            child: Container(
              width: double.infinity,
              height: 180,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(AppImage.heroSection),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(horizontal: w! / 30, vertical: 40),
            decoration: const BoxDecoration(color: Colors.white),
            child: const Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CompanyLogoMobile(logo: AppImage.fb),
                SizedBox(height: 15),
                CompanyLogoMobile(logo: AppImage.google),
                SizedBox(height: 15),
                CompanyLogoMobile(logo: AppImage.cocacola),
                SizedBox(height: 15),
                CompanyLogoMobile(logo: AppImage.linkedIn),
                SizedBox(height: 15),
                CompanyLogoMobile(logo: AppImage.samsung),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget tabletContainer2() {
    return Container(
      height: 700,
      width: double.infinity,
      decoration: BoxDecoration(color: AppColors.primary),
      child: Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                Positioned(
                  top: -20,
                  right: -20,
                  child: Container(
                    width: 320,
                    height: 320,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(AppImage.vector),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: -20,
                  left: -10,
                  child: Container(
                    width: 320,
                    height: 320,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(AppImage.vector1),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: 40,
                  right: 40,
                  bottom: 0,
                  child: Container(
                    width: double.infinity,
                    height: 360,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(AppImage.heroSection),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: w! / 10, vertical: 40),
            decoration: const BoxDecoration(color: Colors.white),
            child: const Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CompanyLogo(logo: AppImage.fb),
                    CompanyLogo(logo: AppImage.google),
                    CompanyLogo(logo: AppImage.cocacola),
                  ],
                ),
                SizedBox(height: 40),
                Row(
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CompanyLogo(logo: AppImage.linkedIn),
                    CompanyLogo(logo: AppImage.samsung),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget desktopContainer2() {
    return Container(
      height: 750,
      width: double.infinity,
      decoration: BoxDecoration(color: AppColors.primary),
      child: Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                Positioned(
                  top: -20,
                  right: -20,
                  child: Container(
                    width: 320,
                    height: 320,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(AppImage.vector),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: -20,
                  left: -10,
                  child: Container(
                    width: 320,
                    height: 320,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(AppImage.vector1),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: 40,
                  right: 40,
                  bottom: 0,
                  child: Container(
                    width: double.infinity,
                    height: 612,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(AppImage.heroSection),
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: w! / 10, vertical: 40),
            decoration: const BoxDecoration(color: Colors.white),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CompanyLogo(logo: AppImage.fb),
                CompanyLogo(logo: AppImage.google),
                CompanyLogo(logo: AppImage.cocacola),
                CompanyLogo(logo: AppImage.linkedIn),
                CompanyLogo(logo: AppImage.samsung),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class CompanyLogo extends StatelessWidget {
  final String logo;
  const CompanyLogo({
    super.key,
    required this.logo,
  });

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      logo,
      width: 159,
      height: 32,
      fit: BoxFit.contain,
    );
  }
}

class CompanyLogoMobile extends StatelessWidget {
  final String logo;
  const CompanyLogoMobile({
    super.key,
    required this.logo,
  });

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      logo,
      width: 180,
      height: 25,
      fit: BoxFit.contain,
    );
  }
}
