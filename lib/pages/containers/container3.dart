import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:website_demo/utils/image_text.dart';
import 'package:website_demo/widgets/common_container.dart';

class Container3 extends StatefulWidget {
  const Container3({super.key});

  @override
  State<Container3> createState() => _Container3State();
}

class _Container3State extends State<Container3> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      desktop: (context) => desktopContainer3(),
      mobile: (context) => mobileContainer3(),
      tablet: (context) => tabletContainer3(),
    );
  }

  Widget mobileContainer3() {
    return commonContainerMobile(
        "Alwalys online",
        "Real-time support with cloud",
        "Tellus lacus morbi sagittis lacus in. Amet nisl at mauris enim accumsan nisi, tincidunt vel. Enim ipsum, amet quis ullamcorper eget ut.",
        AppImage.cloudSection);
  }

  Widget tabletContainer3() {
    return commonContainer(
        "Alwalys online",
        "Real-time support with cloud",
        "Tellus lacus morbi sagittis lacus in. Amet nisl at mauris enim accumsan nisi, tincidunt vel. Enim ipsum, amet quis ullamcorper eget ut.",
        AppImage.cloudSection,
        false);
  }

  Widget desktopContainer3() {
    return commonContainer(
        "Alwalys online",
        "Real-time support with cloud",
        "Tellus lacus morbi sagittis lacus in. Amet nisl at mauris enim accumsan nisi, tincidunt vel. Enim ipsum, amet quis ullamcorper eget ut.",
        AppImage.cloudSection,
        false);
  }
}
