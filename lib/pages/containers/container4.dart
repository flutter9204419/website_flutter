import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:website_demo/utils/image_text.dart';
import 'package:website_demo/widgets/common_container.dart';

class Container4 extends StatefulWidget {
  const Container4({super.key});

  @override
  State<Container4> createState() => _Container4State();
}

class _Container4State extends State<Container4> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      desktop: (context) => desktopContainer4(),
      mobile: (context) => mobileContainer4(),
      tablet: (context) => tabletContainer4(),
    );
  }

  Widget mobileContainer4() {
    return commonContainerMobile(
        "free some cost",
        "Save cost for you and family",
        "Tellus lacus morbi sagittis lacus in. Amet nisl at mauris enim accumsan nisi, tincidunt vel. Enim ipsum, amet quis ullamcorper eget ut..",
        AppImage.familySection);
  }

  Widget tabletContainer4() {
    return commonContainer(
        "free some cost",
        "Save cost for you and family",
        "Tellus lacus morbi sagittis lacus in. Amet nisl at mauris enim accumsan nisi, tincidunt vel. Enim ipsum, amet quis ullamcorper eget ut..",
        AppImage.familySection,
        true);
  }

  Widget desktopContainer4() {
    return commonContainer(
        "free some cost",
        "Save cost for you and family",
        "Tellus lacus morbi sagittis lacus in. Amet nisl at mauris enim accumsan nisi, tincidunt vel. Enim ipsum, amet quis ullamcorper eget ut..",
        AppImage.familySection,
        true);
  }
}
