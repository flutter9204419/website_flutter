import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:website_demo/utils/image_text.dart';
import 'package:website_demo/widgets/common_container.dart';

class Container5 extends StatefulWidget {
  const Container5({super.key});

  @override
  State<Container5> createState() => _Container5State();
}

class _Container5State extends State<Container5> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      desktop: (context) => desktopContainer5(),
      mobile: (context) => mobileContainer5(),
      tablet: (context) => tabletContainer5(),
    );
  }

  Widget mobileContainer5() {
    return commonContainerMobile(
        "Use anytime",
        "Use anytime when you need",
        "Tellus lacus morbi sagittis lacus in. Amet nisl at mauris enim accumsan nisi, tincidunt vel. Enim ipsum, amet quis ullamcorper eget ut.",
        AppImage.needSection);
  }

  Widget tabletContainer5() {
    return commonContainer(
        "Use anytime",
        "Use anytime when you need",
        "Tellus lacus morbi sagittis lacus in. Amet nisl at mauris enim accumsan nisi, tincidunt vel. Enim ipsum, amet quis ullamcorper eget ut.",
        AppImage.needSection,
        false);
  }

  Widget desktopContainer5() {
    return commonContainer(
        "Use anytime",
        "Use anytime when you need",
        "Tellus lacus morbi sagittis lacus in. Amet nisl at mauris enim accumsan nisi, tincidunt vel. Enim ipsum, amet quis ullamcorper eget ut.",
        AppImage.needSection,
        false);
  }
}
