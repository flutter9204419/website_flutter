import 'package:flutter/material.dart';
import 'package:website_demo/utils/colors.dart';

ButtonStyle borderButtonStyle = ButtonStyle(
  elevation: MaterialStateProperty.all(0),
  backgroundColor: MaterialStateProperty.all(Colors.white),
  padding:
      MaterialStateProperty.all(const EdgeInsets.symmetric(horizontal: 15)),
  // side: MaterialStateProperty.all(
  //   BorderSide(
  //     color: AppColors.primary,
  //     width: 1,
  //     style: BorderStyle.solid,
  //   ),
  // ),
  shape: MaterialStateProperty.all(
    RoundedRectangleBorder(
      side: BorderSide(color: AppColors.primary),
      borderRadius: BorderRadius.circular(10),
    ),
  ),
);

ButtonStyle container1ButtonStyle = ButtonStyle(
    backgroundColor: MaterialStateProperty.all(AppColors.primary),
    iconColor: MaterialStateProperty.all(Colors.white),
    shape: MaterialStateProperty.all(
      RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
          side: BorderSide(color: AppColors.primary)),
    ),
    padding:
        MaterialStateProperty.all(const EdgeInsets.symmetric(horizontal: 15)),
    elevation: MaterialStateProperty.all(0));
