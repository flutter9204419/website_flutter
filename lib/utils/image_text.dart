class AppImage {
  static const String logo = "assets/images/Logo.png";
  static const String fb = "assets/images/fb.png";
  static const String google = "assets/images/google.png";
  static const String linkedIn = "assets/images/in.png";
  static const String cocacola = "assets/images/coca.png";
  static const String samsung = "assets/images/samsung.png";
  static const String heroSection = "assets/images/hero_image.png";
  static const String mainSection = "assets/images/Illustrator3.png";
  static const String cloudSection = "assets/images/Illustrator.png";
  static const String familySection = "assets/images/Illustrator2.png";
  static const String needSection = "assets/images/Illustrator1.png";
  static const String laye1 = "assets/images/Layer1.png";
  static const String layer2 = "assets/images/Layer2.png";
  static const String vector = "assets/images/vector.png";
  static const String vector1 = "assets/images/vector1.png";
}
